import Container from '@mui/material/Container'
import Grid from '@mui/material/Grid'
import { Box } from '@mui/system'
import Banner from './_banner'

export interface IProductBanner {
    imgUrl?: string
    title?: string
    content?: string
}

const Index = () => {
    const productBanners: IProductBanner[] = [
        {
            imgUrl: "https://bazaar.ui-lib.com/assets/images/products/nike-black.png", title: "50% Off For Your First Shopping", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis lobortis consequat eu, quam etiam at quis ut convalliss."
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/assets/images/products/nike-black.png", title: "50% Off For Your First Shopping", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis lobortis consequat eu, quam etiam at quis ut convalliss."
        }
    ]

    return (
        <Box width={1519.2} height={520} mb={"60px"} bgcolor="white">
            <Container maxWidth={false} sx={{ width: 1280, height: 520, py: "32px", px: "24px" }}>
                <Banner productBanners={productBanners} />
            </Container>
        </Box>
    )
}

export default Index
