import { Box, Card, CardContent, Typography } from '@mui/material'
import React, { FC } from 'react'
import { ICompanyCar as IBrandOpticsWatch } from "../Car"

interface PropsBrandsOpticsWatch {
    brandsOpticsWatch: IBrandOpticsWatch[],
    handleClick: () => void
}

const styleCardContentHeader = {
    padding: "20px",
    "&:last-child": { paddingBottom: "20px" }
}

const styleBoxHeader = {
    display: "flex",
    mt: "-8px",
    mb: "8px",
    justifyContent: "space-between"
}

const styleTextBrands = {
    fontSize: "20px",
    fontWeight: 600,
    lineHeight: 1.5,
    py: "8px",
    px: "16px",
    color: "#2B34450",
    textTransform: "none",
    whiteSpace: "normal",
    cursor: "pointer"
}

const styleSeparatorCharacter = {
    fontSize: "20px",
    fontWeight: 600,
    lineHeight: 1.3,
    pt: "8px",
    color: "#DAE1E7",
    textTransform: "none",
    whiteSpace: "normal",
}

const styleTextShops = {
    fontSize: "20px",
    fontWeight: 600,
    lineHeight: 1.5,
    py: "8px",
    px: "16px",
    color: "#7D879C",
    textTransform: "none",
    whiteSpace: "normal",
    cursor: "pointer"
}

const styleCardContentBrandsOpticItem = {
    display: "flex",
    width: 168,
    height: 20,
    mb: "12px",
    bgcolor: "#F6F9FC",
    py: "12px",
    color: "#2B3445",
    borderRadius: "5px",
    gap: "1rem",
    alignItems: "center",
    "&:last-child": { paddingBottom: "12px" }
}

const styleTextBrandsOpticItem = { 
    fontSize: 17, 
    fontWeight: 600, 
    lineHeight: 1, 
    textTransform: "capitalize" 
}

const styleCardContentViewAllBrands = {
    width: 168,
    height: 20,
    bgcolor: "#F6F9FC",
    borderRadius: "5px",
    pt: "12px",
    "&:last-child": { paddingBottom: "12px" },
    mt: "64px"
}

const styleTextViewAllBrands = {
    fontSize: 17,
    fontWeight: 600,
    lineHeight: 1,
    textTransform: "capitalize",
    textAlign: "center"
}

const BrandsOpticsWatch: FC<PropsBrandsOpticsWatch> = (props) => {
    const { brandsOpticsWatch, handleClick } = props
    return (
        <Card sx={{ minWidth: 240, height: 462 }}>
            <CardContent
                sx={styleCardContentHeader}>
                <Box sx={styleBoxHeader}>
                    <Typography variant="h3" sx={styleTextBrands}>Brands</Typography>
                    <Typography variant="h3" sx={styleSeparatorCharacter}>|</Typography>
                    <Typography variant="h3" sx={styleTextShops} onClick={handleClick}>Shops</Typography>
                </Box>

                {brandsOpticsWatch.map((brandsOpticItem, index) => {
                    return (
                        <CardContent key={`${brandsOpticItem.name}-${index}`} sx={styleCardContentBrandsOpticItem}>
                            <Box sx={{ width: 20, height: 20 }}>
                                <img style={{ width: "100%", height: "100%" }} src={brandsOpticItem.imgUrl} />
                            </Box>
                            <Typography variant="h4" sx={styleTextBrandsOpticItem}>{brandsOpticItem.name}</Typography>
                        </CardContent>
                    )
                })}
                <CardContent sx={styleCardContentViewAllBrands}>
                    <Typography variant="h4" sx={styleTextViewAllBrands}>View All Brands</Typography>
                </CardContent>
            </CardContent>
        </Card>
    )
}

export default BrandsOpticsWatch
