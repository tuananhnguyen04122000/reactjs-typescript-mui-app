import React, { useEffect, useState } from 'react'
import { Box, Container } from '@mui/material'
import BrandsOpticsWatch from './brands-opticswatch'
import ProductOpticsWatch from './product-opticswatch'

import { ICompanyCar as IBrandOpticsWatch } from "../Car"
import { IProductFlashDeal as IProductOpticsWatch } from "../FlashDeals"
import Shops from '../MobilePhones/shops'
import { IShop } from '../MobilePhones'
import axios from 'axios'

const Index = () => {
    const brandsOpticsWatch: IBrandOpticsWatch[] = [
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbrands%2Fray-ban.png&w=32&q=75", name: "Ray-Ban"
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbrands%2Fzeiss.png&w=32&q=75", name: "Zeiss"
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbrands%2Foccular.png&w=32&q=75", name: "Occular"
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbrands%2Fapple.png&w=32&q=75", name: "Apple"
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbrands%2Ftitan.png&w=32&q=75", name: "Titan"
        },
    ]

    const shops: IShop[] = [
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fshops%2Fherman%20miller.png&w=32&q=75", name: "Keyboard Kiosk",
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fshops%2Fotobi.png&w=32&q=75", name: "Anytime Buys"
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fshops%2Fhatil.png&w=32&q=75", name: "Word Wide Wishes"
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fshops%2Fsteelcase.png&w=32&q=75", name: "Cybershop"
        }
    ]

    // const productOpticsWatch: IProductOpticsWatch[] = [
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FAccessories%2F7.PoliceGrayEyeglasses.png&w=1920&q=75", "name": "Police Gray Eyeglasses", "price": 150, "oldPrice": 167, "discount": 1, "id": "41"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FAccessories%2F8.RayBanMattBlack.png&w=1920&q=75", "name": "Say Ban Matt Black", "price": 117, "oldPrice": 129, "discount": 9, "id": "42"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FAccessories%2F9.RayBanBlack.png&w=1920&q=75", "name": "Say Ban Black", "price": 156, "oldPrice": 168, "discount": 7, "id": "43"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FAccessories%2F10.RayBanOcean.png&w=1920&q=75", "name": "Say Ban Ocean", "price": 194, "oldPrice": 211, "discount": 8, "id": "44"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FAccessories%2F11.SunglassesCollection.png&w=1920&q=75", "name": "Sun glasses Collection", "price": 136, "oldPrice": 147, "discount": 7, "id": "45"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FAccessories%2F12.Xiaomimiband2.png&w=1920&q=75", "name": "Ziaomi mi band2", "price": 222, "oldPrice": 239, "discount": 7, "id": "46"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FAccessories%2F13.FossilWatchBrown.png&w=1920&q=75", "name": "Kossil Watch Brown", "price": 244, "oldPrice": 266, "discount": 8, "id": "47"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FAccessories%2F14.MVMTMWatchBlack.png&w=1920&q=75", "name": "MVMTM Watch Black", "price": 199, "oldPrice": 212, "discount": 6, "id": "48"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FAccessories%2F15.BarihoWatchBlack.png&w=1920&q=75", "name": "Xarioho Watch Black", "price": 214, "oldPrice": 226, "discount": 5, "id": "49"
    //     }
    // ]

    const [showBrandsOpticsWatch, setShowBrandsOpticsWatch] = useState<boolean>(true);

    const [productOpticsWatch, setProductOpticsWatch] = useState<IProductOpticsWatch[]>([])

    useEffect(() => {
        fetchData()
    }, [])

    async function fetchData() {
        try {
            await axios.get('https://6402f090f61d96ac4873acc5.mockapi.io/api/product').then((result) => {
                setProductOpticsWatch(result.data[0].opticsWatch);
            });

        } catch (errors) {
            console.log(errors)
        }
    }

    const toggleComponents = () => {
        setShowBrandsOpticsWatch(!showBrandsOpticsWatch);
    };

    return (
        <Container maxWidth={false} sx={{ width: 1280, height: 1330.97, mb: "80px" }}>
            <Box sx={{ display: "flex", gap: "1.75rem" }}>
                {
                    (showBrandsOpticsWatch) ?
                        <BrandsOpticsWatch brandsOpticsWatch={brandsOpticsWatch} handleClick={toggleComponents} />
                        :
                        <Shops shops={shops} handleClick={toggleComponents} />
                }
                <ProductOpticsWatch productOpticsWatch={productOpticsWatch} />
            </Box>
        </Container>
    )
}

export default Index
