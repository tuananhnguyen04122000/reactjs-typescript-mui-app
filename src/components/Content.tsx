import Banner from "./Banner"
import BigDiscounts from "./BigDiscounts"
import Car from "./Car"
import Categories from "./Categories"
import FlashDeals from "./FlashDeals"
import MobilePhones from "./MobilePhones"
import MoreForYou from "./MoreForYou"
import NewArrivals from "./NewArrivals"
import OpticsWatch from "./OpticsWatch"
import Recommend from "./Recommend"
import Service from "./Service"
import TopCategories from "./TopCategories"
import TopRatingAndFeaturedBrands from "./TopRatingAndFeaturedBrands"

const Content = () => {
  return (
    <>
      <Banner />
      <FlashDeals />
      <TopCategories />
      <TopRatingAndFeaturedBrands />
      <NewArrivals />
      <BigDiscounts />
      <Car />
      <MobilePhones />
      <Recommend />
      <OpticsWatch />
      <Categories />
      <MoreForYou />
      <Service />
    </>
  )
}

export default Content
