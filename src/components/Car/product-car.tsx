import { Box, Card, Chip, Grid, IconButton, Rating, Typography } from '@mui/material'
import React, { FC } from 'react'
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import AddIcon from '@mui/icons-material/Add';
import { Link } from 'react-router-dom'
import { IProductFlashDeal as IProductCar } from "../FlashDeals/index"
import AddToCartButton from '../FlashDeals/AddToCartButton';

interface PropsProductCar {
  productCars: IProductCar[]
}

const ProductCar: FC<PropsProductCar> = (props) => {
  const { productCars } = props;

  return (
    <Box>
      <Box sx={{ display: "flex", width: 964, height: 25, mb: "24px", justifyContent: "space-between" }}>
        <Typography fontSize={25} sx={{ fontWeight: 700, lineHeight: 1 }}>Cars</Typography>
        <Link to={"/"} style={{
          height: 21,
          alignSelf: "center",
          textDecoration: "none"
        }}>
          <Box display={"flex"} alignItems={"center"}>
            <Typography fontSize={14}>View all</Typography>
            <ArrowRightIcon fontSize="inherit" />
          </Box>
        </Link>
      </Box>

      <Grid container spacing={3}>
        {productCars.map((productCarItem, index) => {
          return (
            <Grid key={`${productCarItem.name}-${index}`} item xs={12} sm={6} lg={4}>
              <Card>
                <Box>
                  <Chip
                    label={`${productCarItem.discount}% off`}
                    color={"error"}
                    size={"small"}
                    sx={{
                      position: "absolute",
                      fontWeight: 600,
                      fontSize: 10,
                      cursor: "default",
                      mt: 1,
                      ml: 1,
                      zIndex: 1,
                    }}
                  />

                  <Link to={`/product/${productCarItem.id}`} style={{ textDecoration: "none" }}>
                    <Box sx={{ width: 305.325, height: 305.325 }}>
                      <img style={{ width: "100%", height: "100%" }} src={productCarItem.imgUrl} />
                    </Box>
                  </Link>

                  <Box>
                    <Box sx={{ display: "flex", p: "16px", height: 77 }}>
                      <Box>
                        <Box sx={{ width: 235.325, mr: "8px" }}>
                          <Link to="/" style={{ textDecoration: "none" }}>
                            <Box component={"h4"} sx={{ height: 21, mt: "0px", mb: "4px", }}>
                              <Typography sx={{ fontSize: 14, fontWeight: 500 }}>{productCarItem.name}</Typography>
                            </Box>
                            <Rating defaultValue={5} color={"warn"} sx={{ fontSize: "1.25rem" }} readOnly />
                          </Link>
                          <Box sx={{ display: "flex", mt: "4px", gap: "8px" }}>
                            <Typography component={"div"} sx={{ color: "#D23F57", fontSize: 14, lineHeight: 1.5, fontWeight: 600, textTransform: "none", whiteSpace: "normal" }}>{productCarItem.price} US$</Typography>
                            <Typography component={"div"} sx={{ color: "#7D879C", fontSize: 14, lineHeight: 1.5, fontWeight: 600, textTransform: "none", whiteSpace: "normal" }} ><del>{productCarItem.oldPrice} US$</del></Typography>
                          </Box>
                        </Box>
                      </Box>

                      <AddToCartButton item={productCarItem}/>
                    </Box>
                  </Box>
                </Box>
              </Card>
            </Grid>
          )
        })}
      </Grid>
    </Box>
  )
}

export default ProductCar
