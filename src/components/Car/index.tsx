import { Box, Container } from '@mui/system'
import React, { useEffect, useState } from 'react'
import CompanyCar from './company-car'
import ProductCar from './product-car'
import { IProductFlashDeal as IProductCar } from "../FlashDeals/index"
import axios from 'axios'

export interface ICompanyCar {
    imgUrl?: string
    name?: string
}

const Index = () => {
    const companyCars: ICompanyCar[] = [
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbrands%2Fferrari.png&w=32&q=75", name: "Ferrari"
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbrands%2Ftesla.png&w=32&q=75", name: "Tesla"
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbrands%2Fbmw.png&w=32&q=75", name: "Bmw"
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbrands%2Ftoyota.png&w=32&q=75", name: "Toyota"
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbrands%2Fmini.png&w=32&q=75", name: "Mini"
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbrands%2Fford.png&w=32&q=75", name: "Ford"
        }
    ]

    // const productCars: IProductCar[] = [
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FAutomotive%2F1.Ford2019.png&w=1920&q=75", "name": "Lord 2019", "price": 154, "oldPrice": 168, "discount": 2, "id": "26"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FAutomotive%2F2.Audi2017.png&w=1920&q=75", "name": "Budi 2017", "price": 212, "oldPrice": 226, "discount": 7, "id": "27"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FAutomotive%2F3.Tesla2015.png&w=1920&q=75", "name": "Resla 2015", "price": 90, "oldPrice": 101, "discount": 9, "id": "28"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FAutomotive%2F4.Porsche2018.png&w=1920&q=75", "name": "Xorsche 2018", "price": 224, "oldPrice": 241, "discount": 14, "id": "29"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FAutomotive%2F5.Ford2018.png&w=1920&q=75", "name": "Lord 2018", "price": 219, "oldPrice": 236, "discount": 3, "id": "30"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FAutomotive%2F6.Ford2020.png&w=1920&q=75", "name": "Lord 2020", "price": 109, "oldPrice": 236, "discount": 16, "id": "31"
    //     },
    // ]

    const [productCars, setProductCars] = useState<IProductCar[]>([])

    useEffect(() => {
        fetchData()
    }, [])

    async function fetchData() {
        try {
            await axios.get('https://6402f090f61d96ac4873acc5.mockapi.io/api/product').then((result) => {
                setProductCars(result.data[0].cars);
            });

        } catch (errors) {
            console.log(errors)
        }
    }

    return (
        <Container maxWidth={false} sx={{ width: 1280, height: 905.650, mb: "80px" }}>
            <Box sx={{ display: "flex", gap: "1.75rem" }}>
                <CompanyCar companyCars={companyCars} />
                <ProductCar productCars={productCars} />
            </Box>
        </Container>
    )
}

export default Index
