import Content from '../Content'
import Footer from './Footer'
import Header from './Header'

const Index = () => {
    return (
        <>
            <Header />
            <Content />
            <Footer />
        </>
    )
}

export default Index
