import { createSvgIcon, IconButton, Typography } from '@mui/material'
import FacebookOutlinedIcon from '@mui/icons-material/FacebookOutlined';
import YouTubeIcon from '@mui/icons-material/YouTube';
import GoogleIcon from '@mui/icons-material/Google';
import InstagramIcon from '@mui/icons-material/Instagram';
import TwitterIcon from '@mui/icons-material/Twitter';
import Container from '@mui/material/Container'
import Grid from '@mui/material/Grid'
import { Box } from '@mui/system'

const GooglePlayIcon = createSvgIcon(
  <g>
    <path d="M17.9236 8.23227C15.1356 6.67287 10.6607 4.168853.71978 0.282486C3.25183 -0.02656142.71342 -0.0670615 2.24823 0.090392L14.1569 11.999L17.9236 8.23227Z" fill='#32BBFF' />
    <path d="M2.24824 0.090332C2.16105 0.119863 2.07621 0.155488 1.99507 0.19852C1.48137 0.476395 1.10388 1.0111 1.10388 1.68737V22.3105C1.10388 22.9868 1.48132 23.5215 1.99507 23.7993C2.07607 23.8423 2.16087 23.8781 2.24796 23.9078L14.1568 11.999L2.24824 0.090332Z" fill="#32BBFF" />
    <path d="M14.1569 11.999L2.24799 23.9079C2.71331 24.0665 3.25172 24.0291 3.71982 23.7155C10.451 19.9463 14.8754 17.472 17.6957 15.8993C17.7742 15.8552 17.8512 15.8121 17.9272 15.7694L14.1569 11.999Z" fill="#32BBFF" />
    <path d="M1.10388 11.999V22.3106C1.10388 22.9869 1.48132 23.5216 1.99507 23.7994C2.07607 23.8424 2.16087 23.8782 2.24796 23.9079L14.1568 11.999H1.10388Z" fill="#2C9FD9"></path>
    <path d="M3.71978 0.282559C3.16327 -0.0843794 2.50876 -0.0739263 1.99506 0.198699L13.9761 12.1798L17.9235 8.23234C15.1356 6.67295 10.6607 4.16893 3.71978 0.282559Z" fill="#29CC5E"></path>
    <path d="M13.9762 11.8184L1.99506 23.7994C2.50881 24.072 3.16327 24.0877 3.71978 23.7155C10.4509 19.9463 14.8754 17.472 17.6957 15.8993C17.7742 15.8552 17.8511 15.8121 17.9271 15.7694L13.9762 11.8184Z" fill="#D93F21"></path>
    <path d="M22.8961 11.9989C22.8961 11.4275 22.6078 10.8509 22.0363 10.5311C22.0363 10.5311 20.9676 9.93479 17.6919 8.10254L13.7955 11.9989L17.6957 15.8992C20.9355 14.0801 22.0363 13.4667 22.0363 13.4667C22.6078 13.147 22.8961 12.5703 22.8961 11.9989Z" fill="#FFD500"></path>
    <path d="M22.0363 13.4669C22.6078 13.1471 22.8961 12.5704 22.8961 11.999H13.7955L17.6957 15.8993C20.9355 14.0802 22.0363 13.4669 22.0363 13.4669Z" fill="#FFAA00"></path>
  </g>, "GooglePlay"
)

const AppStoreIcon = createSvgIcon(
  <g>
    <path d="M19.0781 24H4.92188C2.20795 24 0 21.792 0 19.0781V4.92188C0 2.20795 2.20795 0 4.92188 0H19.0781C21.792 0 24 2.20795 24 4.92188V19.0781C24 21.792 21.792 24 19.0781 24Z" fill="#00C3FF"></path>
    <path d="M19.0781 0H12V24H19.0781C21.792 24 24 21.792 24 19.0781V4.92188C24 2.20795 21.792 0 19.0781 0Z" fill="#00AAF0"></path>
    <path d="M7.38978 18.4929C7.17299 18.8684 6.77825 19.0785 6.37292 19.0786C6.17422 19.0786 5.97303 19.0281 5.78895 18.9218C5.22936 18.5988 5.03694 17.8807 5.36 17.3211L5.59049 16.9219H8.29681L7.38978 18.4929Z" fill="white"></path>
    <path d="M5.17969 15.984C4.53352 15.984 4.00781 15.4583 4.00781 14.8122C4.00781 14.166 4.53352 13.6323 5.17969 13.6323H7.48491L10.6468 8.1637L9.78989 6.67945C9.46683 6.11985 9.6592 5.32173 10.2188 5.07862H10.2188C10.7784 4.75556 11.4966 4.94803 11.8196 5.50757L12 5.81999L12.1804 5.50762C12.5036 4.94798 13.2217 4.7556 13.7812 5.07867C14.0522 5.23518 14.2462 5.48788 14.3272 5.79023C14.3282 6.09257 14.3666 6.32842 14.2101 6.67949L10.1912 13.6323H12.7262L14.0794 15.984H5.17969Z" fill="white"></path>
    <path d="M18.8203 15.9844H17.8682L18.6399 17.3211C18.963 17.8806 18.7706 18.5988 18.2111 18.9219C18.0304 19.0261 17.83 19.0794 17.6269 19.0794C17.5251 19.0794 17.4228 19.066 17.3217 19.039C17.0194 18.9579 16.7667 18.764 16.6101 18.4929L12.5412 11.4453L13.8943 9.10156L16.5151 13.6327H18.8203C19.4665 13.6327 19.9922 14.1664 19.9922 14.8126C19.9922 15.4587 19.4665 15.9844 18.8203 15.9844Z" fill="#F2F2F2"></path>
    <path d="M12 13.6326V15.9844H14.0794L12.7262 13.6326H12Z" fill="#F2F2F2"></path>
    <path d="M14.3272 5.7907C14.2461 5.48836 14.0522 5.23561 13.7812 5.07914C13.2217 4.75608 12.5035 4.9484 12.1804 5.50809L12 5.82047V10.508L14.2101 6.67996C14.3666 6.32889 14.3282 6.09309 14.3272 5.7907Z" fill="#F2F2F2"></path>
  </g>, "AppStore"
)

const MyFacebookIcon = createSvgIcon(
  <path d="M1.72081 14H4.59374V8.09799H7.18233L7.46667 5.16509H4.59374V3.68421C4.59374 3.27726 4.9153 2.94737 5.31197 2.94737H7.46667V0H5.31197C3.32863 0 1.72081 1.64948 1.72081 3.68421V5.16509H0.284341L0 8.09799H1.72081V14Z" fill="currentColor"></path>
  , "MyFacebookIcon"
)

const Index = () => {
  return (
    <Box sx={{ height: 404.5, backgroundColor: "#222935" }}>
      <Container maxWidth={false} sx={{
        width: 1280,
        height: 324.5,
        display: "grid",
        paddingY: 2,
        gridTemplateAreas: `""`
      }}>
        <Box sx={{
          width: 1232,
          height: 372.5,
          display: "flex",
          alignItems: "center"
        }}>
          <Grid container spacing={3} width={1256} height={236.5}>
            <Grid item xs={12} sm={6} md={6} lg={4}>
              <Box width={105} height={50} mb={"20px"}>
                <img src='https://bazaar.ui-lib.com/assets/images/logo.svg' />
              </Box>
              <Typography color={"#AEB4BE"} fontSize={14} mb={"20px"}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Auctor libero id et, in gravida. Sit diam duis mauris nulla cursus. Erat et lectus vel ut sollicitudin elit at amet.
              </Typography>
              <Box sx={{ display: "flex", width: 410.66, height: 65, margin: "-8px" }}>
                <Box sx={{ width: 161.05, height: 65 }}>
                  <Box sx={{
                    display: "grid",
                    width: 113.030,
                    height: 29,
                    py: "10px",
                    px: "16px",
                    margin: "8px",
                    alignItems: "center",
                    gridTemplateAreas: `"logo-google-play . google-play"`,
                    backgroundColor: "black",
                    borderRadius: 1,
                  }}>
                    <GooglePlayIcon sx={{ gridArea: "logo-google-play" }} />
                    <Box sx={{ gridArea: "google-play", width: 81.05, height: 29, color: "white" }}>
                      <Box height={8}>
                        <Typography fontSize={8}>Get it on</Typography>
                      </Box>
                      <Box height={21}>
                        <Typography fontSize={14}>Google Play</Typography>
                      </Box>
                    </Box>
                  </Box>
                </Box>
                <Box sx={{ width: 148.1, height: 65 }}>
                  <Box sx={{
                    display: "grid",
                    width: 100.1,
                    height: 29,
                    py: "10px",
                    px: "16px",
                    margin: "8px",
                    alignItems: "center",
                    backgroundColor: "black",
                    borderRadius: 1,
                    gridTemplateAreas: `"logo-appstore . appstore"`,
                  }}>
                    <AppStoreIcon sx={{ gridArea: "logo-appstore" }} />
                    <Box sx={{ gridArea: "appstore", width: 68.1, height: 29, color: "white" }}>
                      <Box height={8}>
                        <Typography fontSize={8}>Download on the</Typography>
                      </Box>
                      <Box height={21}>
                        <Typography fontSize={14}>App Store</Typography>
                      </Box>
                    </Box>
                  </Box>
                </Box>
              </Box>
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={2}>
              <Box sx={{ height: 18, mb: "12px", color: "white" }}>
                <Typography fontSize={18}>About Us</Typography>
              </Box>
              <Box sx={{ height: 153, color: "#AEB4BE", justifyContent: "space-between" }}>
                <Typography fontSize={14} py={"4.8px"}>Careers</Typography>
                <Typography fontSize={14} py={"4.8px"}>Out Stores</Typography>
                <Typography fontSize={14} py={"4.8px"}>Out Cares</Typography>
                <Typography fontSize={14} py={"4.8px"}>Terms & Conditions</Typography>
                <Typography fontSize={14} py={"4.8px"}>Privacy Policy</Typography>
              </Box>
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={3}>
              <Box sx={{ height: 18, mb: "12px", color: "white" }}>
                <Typography fontSize={18}>Customer Care</Typography>
              </Box>
              <Box sx={{ height: 153, color: "#AEB4BE", justifyContent: "space-between" }}>
                <Typography fontSize={14} py={"4.8px"}>Help Center</Typography>
                <Typography fontSize={14} py={"4.8px"}>How to Buy</Typography>
                <Typography fontSize={14} py={"4.8px"}>Track Your Order</Typography>
                <Typography fontSize={14} py={"4.8px"}>Corporate & Bulk Purchasing</Typography>
                <Typography fontSize={14} py={"4.8px"}>Returns & Refunds</Typography>
              </Box>
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={3}>
              <Box sx={{ height: 18, mb: "12px", color: "white" }}>
                <Typography fontSize={18}>Contact Us</Typography>
              </Box>
              <Box sx={{ color: "#AEB4BE", justifyContent: "space-between" }}>
                <Typography fontSize={14} py={"4.8px"}>70 Washington Square South, New York, NY 10012, United States</Typography>
                <Typography fontSize={14} py={"4.8px"}>Email: uilib.help@gmail.com</Typography>
                <Typography fontSize={14} py={"4.8px"} mb={"16px"}>Phone: +1 1123 456 780</Typography>
              </Box>
              <Box sx={{ display: "flex", }}>
                <IconButton sx={{ width: 36, backgroundColor: "black", ':hover': { bgcolor: '#222935' }, p: "4px", mr: "4px" }} >
                  <FacebookOutlinedIcon fontSize='inherit' sx={{ color: "white" }}>
                  </FacebookOutlinedIcon>
                </IconButton>
                <IconButton sx={{ width: 36, backgroundColor: "black", ':hover': { bgcolor: '#222935' }, p: "4px", mr: "4px" }} size='medium' >
                  <TwitterIcon fontSize='small' sx={{ color: "white" }}>
                  </TwitterIcon>
                </IconButton>
                <IconButton sx={{ width: 36, backgroundColor: "black", ':hover': { bgcolor: '#222935' }, p: "4px", mr: "4px" }} size='medium' >
                  <YouTubeIcon fontSize='small' sx={{ color: "white" }}>
                  </YouTubeIcon>
                </IconButton>
                <IconButton sx={{ width: 36, backgroundColor: "black", ':hover': { bgcolor: '#222935' }, p: "4px", mr: "4px" }} size='medium' >
                  <GoogleIcon fontSize='small' sx={{ color: "white" }}>
                  </GoogleIcon>
                </IconButton>
                <IconButton sx={{ width: 36, backgroundColor: "black", ':hover': { bgcolor: '#222935' } }} size='medium' >
                  <InstagramIcon fontSize='small' sx={{ color: "white" }}>
                  </InstagramIcon>
                </IconButton>
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Container >
    </Box >
  )
}

export default Index
