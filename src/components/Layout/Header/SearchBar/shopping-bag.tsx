import { Badge, Box, Button, Drawer, IconButton, List, ListItem, ListItemText, Typography } from '@mui/material'
import ShoppingBagOutlinedIcon from "@mui/icons-material/ShoppingBagOutlined";
import CloseIcon from '@mui/icons-material/Close';
import React, { useState } from 'react'
import { useRecoilValue } from 'recoil';

import ProductItem from './ProductItem';
import { cartState } from '../../../../recoil/Atoms';
import { cartTotalPrice, cartTotalProduct } from '../../../../recoil/Selectors';
import { Link } from 'react-router-dom';


const ShoppingBag = () => {
    const styleBadge = {
        color: "white",
        '& .MuiBadge-badge': {
            backgroundColor: '#D23F57',
        },
    }

    const styleIconButton = {
        p: "10px",
        backgroundColor: "#f3f5f9",
    }
    const styleBox = {
        height: 74,
        mx: "24px",
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        borderBottom: "1px solid #f3f5f9"
    }

    const styleBoxTotalProduct = {
        display: "flex",
        gap: "8px",
        color: "#183c66"
    }

    const styleTextTotalProduct = {
        fontSize: 14,
        lineHeight: 2,
        fontWeight: 600
    }

    const styleButtonCheckout = {
        width: "100%",
        mb: "12px",
        bgcolor: "#d32f2f",
        fontWeight: 550,
        textTransform: "none",
        ":hover": {
            bgcolor: "#e3364e"
        }
    }

    const styleButtonViewCart = {
        width: "100%",
        borderColor: "#d32f2f",
        color: "#d32f2f",
        fontWeight: 550,
        textTransform: "none",
        ":hover": {
            bgcolor: "#fdf7f8",
            borderColor: "#d32f2f",
        }
    }

    const cart = useRecoilValue(cartState)
    const total = useRecoilValue(cartTotalPrice)
    const totalProduct = useRecoilValue(cartTotalProduct)

    const [isDrawerOpen, setIsDrawerOpen] = useState(false);

    const toggleDrawer = (isOpen: boolean) => () => {
        setIsDrawerOpen(isOpen);
    };

    return (
        <Badge badgeContent={totalProduct} sx={styleBadge}>
            <IconButton sx={styleIconButton} onClick={toggleDrawer(true)}>
                <ShoppingBagOutlinedIcon />
            </IconButton>
            <Drawer
                anchor="right"
                open={isDrawerOpen}
                onClose={toggleDrawer(false)}
            >
                <Box sx={styleBox}>
                    <Box sx={styleBoxTotalProduct}>
                        <ShoppingBagOutlinedIcon />
                        <Typography sx={styleTextTotalProduct}>{totalProduct} item</Typography>
                    </Box>
                    <IconButton onClick={toggleDrawer(false)}>
                        <CloseIcon />
                    </IconButton>
                </Box>
                {
                    cart.map((productItem, index) => {
                        return (
                            <ProductItem key={`${productItem}-${index}`} productItem={productItem} />
                        )
                    })
                }

                <Box sx={{ p: "20px", position: "sticky", bottom: 0, zIndex: 1, bgcolor: "white" }}>
                    <Link to={`/checkout`} style={{ textDecoration: "none"}}>
                        <Button variant='contained' sx={styleButtonCheckout}>Checkout Now({total} US$)</Button>
                    </Link>
                    <Button variant='outlined' sx={styleButtonViewCart}>View Cart</Button>
                </Box>

            </Drawer>
        </Badge>
    )
}

export default ShoppingBag
