import FormControl from "@mui/material/FormControl"
import Box from "@mui/material/Box"
import Container from "@mui/material/Container"
import SearchIcon from "@mui/icons-material/Search";
import InputBase from "@mui/material/InputBase";
import React, { useState } from "react"
import { Badge, Button, IconButton, InputAdornment, Menu, MenuItem, TextField, Typography } from "@mui/material";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import { Stack } from "@mui/system";
import KeyboardArrowDown from "@mui/icons-material/KeyboardArrowDown";
import ShoppingBag from "./shopping-bag";
import { Link } from "react-router-dom";


const Index = () => {
    const options = [
        "All Categories",
        "Car",
        "Clothes",
        "Electronics",
        "Laptop",
        "Desktop",
        "Camera",
        "Toys",
    ];

    const styleContainer = {
        height: 80,
        width: 1280,
        display: "grid",
        gridTemplateAreas: `"logo search user"`
    }

    const styleBoxLogo = {
        gridArea: "logo",
        width: 170,
        height: 44,
        marginRight: 2,
        alignSelf: "center"
    }

    const styleBoxSearch = {
        gridArea: "search",
        width: 942,
        height: 44,
        alignSelf: "center"
    }

    const styleSearchIcon = {
        color: "grey",
        mr: "6px"
    }

    const styleInputProps = {
        paddingTop: "8.5px",
        paddingBottom: "8.5px"
    }

    const styleBoxUser = {
        gridArea: "user",
        display: "flex",
        gap: "12px",
        alignSelf: "center"
    }

    const styleButton = {
        display: "flex",
        textTransform: "none",
        width: 210,
        boder: 0,
        outline: 0,
        m: 0,
        borderRadius: 0,
        p: 0,
        px: "24px",
        gap: "4px",
        height: "100%",
        color: "#4B566B",
        backgroundColor: "#F6F9FC",
        alignItems: "center",
        whiteSpace: "pre",
        borderTopRightRadius: "300px",
        borderBottomRightRadius: "300px",
        borderLeft: "1px solid #DAE1E7",
        fontWeight: 400
    }

    const styleKeyboardArrowDown = { color: "grey", }

    const styleInputBase = {
        width: 656,
        height: 44,
        pl: "14px",
        color: "grey",
        border: "1px solid #ccc",
        borderRadius: "1200px",
        "&:hover": {
            borderColor: "#d32f2f",
            "& .MuiButton-root": {
                borderLeftColor: "#ccc",
                borderBottomColor: "#d32f2f",
                borderTopColor: "#d32f2f"
            }
        },
    }

    const styleIconButton = {
        p: "10px",
        backgroundColor: "#f3f5f9",
    }

    const [isSticky, setIsSticky] = useState(false);

    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
    const [selectedIndex, setSelectedIndex] = useState<number>(0);
    const open = Boolean(anchorEl);
    const handleClickListItem = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleScroll = () => {
        const scrollY = window.scrollY;
        setIsSticky(scrollY > 200);
    };

    React.useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, []);

    const handleMenuItemClick = (
        event: React.MouseEvent<HTMLElement>,
        index: number,
    ) => {
        setSelectedIndex(index);
        setAnchorEl(null);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <Box sx={{ bgcolor: "white", position: isSticky ? 'sticky' : 'static', top: isSticky ? 0 : 'auto', boxShadow: 'rgba(43, 52, 69, 0.1) 0px 4px 16px', zIndex: 2 }}>
            <Container maxWidth={false} sx={styleContainer}>

                <Box sx={styleBoxLogo}>
                    <Link to={`/`}>
                        <img width={92.4} src="https://bazaar.ui-lib.com/assets/images/logo2.svg"></img>
                    </Link>
                </Box>

                <Box sx={styleBoxSearch} >
                    <Box sx={{ display: "flex", mx: "136px" }}>
                        <FormControl fullWidth size="small" >
                            <InputBase
                                placeholder="Searching for..."
                                startAdornment={<SearchIcon fontSize="small" sx={styleSearchIcon} />}
                                inputProps={{ style: styleInputProps }}
                                endAdornment={
                                    <>
                                        <Button variant="text" sx={styleButton} onClick={handleClickListItem}>
                                            {options[selectedIndex]}
                                            <KeyboardArrowDown fontSize="small" sx={styleKeyboardArrowDown} />
                                        </Button>
                                        <Menu
                                            id="basic-menu"
                                            anchorEl={anchorEl}
                                            open={open}
                                            onClose={handleClose}
                                            MenuListProps={{
                                                "aria-labelledby": "basic-button",
                                            }}
                                        >
                                            {options.map((option, index) => (
                                                <MenuItem
                                                    key={option}
                                                    selected={index === selectedIndex}
                                                    onClick={(event) => handleMenuItemClick(event, index)}
                                                >
                                                    {option}
                                                </MenuItem>
                                            ))}
                                        </Menu>
                                    </>
                                }
                                sx={styleInputBase}
                            />
                        </FormControl>
                    </Box>
                </Box>
                <Box sx={styleBoxUser}>
                    <IconButton sx={styleIconButton}><PersonOutlineIcon /></IconButton>
                    <ShoppingBag />
                </Box>
            </Container>
        </Box>
    )
}

export default Index
