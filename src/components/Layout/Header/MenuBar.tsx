import { Button, Typography } from "@mui/material"
import Box from "@mui/material/Box"
import Container from "@mui/material/Container"
import WidgetsIcon from "@mui/icons-material/Widgets";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import React from "react"

const MenuBar = () => {
    const options = [
        "Home",
        "Mega Menu",
        "Full Screen Menu",
        "Page",
        "User Account",
        "Vendor Account",
    ]

    const styleBox = { 
        borderBottom: 1, 
        borderColor: "#F3F5F9",
        bgcolor:"white"
    }

    const styleContainer = {
        height: 60,
        width: 1280,
        display: "grid",
        gridTemplateAreas: `"categories . menu-bar"`
    }

    const styleBoxCategories = {
        gridArea: "categories",
        width: 278,
        height: 40,
        alignSelf: "center",
        justifyContent: "center",
        backgroundColor: "#f6f9fc",
    }

    const styleButtonCategories = {
        textTransform: "none",
        width: 278,
        height: 40,
        color: "grey"
    }

    const styleTextButtonCategories = {
        width: 212,
        textAlign: "left",
        color: "#7D879C",
        fontSize: 14
    }

    const styleBoxOption = {
        display: "flex",
        ":hover": { color: "#d32f2f", cursor: "pointer" }
    }

    const styleBoxOptions = {
        gridArea: "menu-bar",
        display: "flex",
        alignSelf: "center",
        justifyContent: "space-between",
    }

    return (
        <Box sx={styleBox}>
            <Container maxWidth={false} sx={styleContainer}>
                <Box sx={styleBoxCategories}>
                    <Button sx={styleButtonCategories} startIcon={<WidgetsIcon sx={{ color: "black" }} />}>
                        <Typography sx={styleTextButtonCategories}>Categories</Typography>
                        <KeyboardArrowRightIcon sx={{ color: "black" }} />
                    </Button>
                </Box>
                <Box sx={styleBoxOptions}>
                    {options.map((option, index) => {
                        return (
                            <Box sx={styleBoxOption} key={option}>
                                <Typography sx={{ fontSize: 14 }}>{options[index]}</Typography>
                                <KeyboardArrowDownIcon fontSize="small" sx={{ color: "grey" }} />
                            </Box>
                        )
                    })}
                </Box>
            </Container>
        </Box>
    )
}

export default MenuBar
