import Stack from "@mui/material/Stack"
import Box from "@mui/material/Box"
import Container from "@mui/system/Container"
import Chip from "@mui/material/Chip"
import { Button, Typography } from "@mui/material"
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import TwitterIcon from "@mui/icons-material/Twitter";
import FacebookIcon from "@mui/icons-material/Facebook";
import InstagramIcon from "@mui/icons-material/Instagram";
import SearchBar from "./SearchBar"
import MenuBar from "./MenuBar"

const Index = () => {
    return (
        <>
            <Box sx={{ height: 40, backgroundColor: "black" }}>
                <Container maxWidth={false} sx={{
                    height: 40,
                    width: 1280,
                    display: "grid",
                    gridTemplateAreas: `"title top-bar-right"`
                }}>
                    <Box sx={{
                        gridArea: "title",
                        display: "flex",
                        alignSelf: "center",
                        alignItems: "center"
                    }}>
                        <Chip label={"HOT"} color={"error"} size={"small"} sx={{ marginRight: 1, width: 53.43, height: 19.5 }} />
                        <Typography color={"white"} fontSize={12}>Free Express Shipping</Typography>
                    </Box>
                    <Box sx={{ gridArea: "top-bar-right", display: "flex", justifyContent: "end" }}>
                        <Button sx={{
                            minWidth: 38.1,
                            color: "white",
                            fontSize: 12,
                            marginRight: 2,
                            padding: 0
                        }}>
                            EN
                            <ExpandMoreIcon fontSize="inherit" sx={{ color: "white" }}></ExpandMoreIcon>
                        </Button>
                        <Box sx={{
                            display: "flex",
                            width: 72,
                            alignItems: "center",
                            justifyContent: "space-between"
                        }}>
                            <TwitterIcon fontSize="inherit" sx={{ color: "white" }} />
                            <FacebookIcon fontSize="inherit" sx={{ color: "white" }} />
                            <InstagramIcon fontSize="inherit" sx={{ color: "white" }} />
                        </Box>
                    </Box>
                </Container>
            </Box>
            <SearchBar />
            <MenuBar />
        </>
    )
}

export default Index
