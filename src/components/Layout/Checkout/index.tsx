import { Grid } from '@mui/material'
import { Container } from '@mui/system'
import React, { useEffect } from 'react'
import { useRecoilState, useRecoilValue } from 'recoil'
import { cartState } from '../../../recoil/Atoms'
import { cartTotalPrice } from '../../../recoil/Selectors'
import Header from '../Header'
import PaymentDetails from './PaymentDetails'
import YourOrder from './YourOrder'
import { useNavigate  } from 'react-router-dom'
const Index = () => {
    const navigate = useNavigate();
    const styleContainer = {
        width: 1280,
        height: "auto",
        my: "24px"
    }

    const [cart, setCart] = useRecoilState(cartState)
    const subtotal = useRecoilValue(cartTotalPrice)

    useEffect(() => {
        const storedCart = localStorage.getItem('cart')
        storedCart && setCart(JSON.parse(storedCart))
    }, [])
    useEffect(()=>{
        if(cart?.length===0)
        navigate("/");
       },[cart])
    return (
        <>
            <Header />
            <Container maxWidth={false} sx={styleContainer}>
                <Grid container spacing={3}>
                    <Grid item xs={12} md={8} lg={8}>
                        <PaymentDetails />
                    </Grid>
                    <Grid item xs={12} md={4} lg={4}>
                        <YourOrder cart={cart} subtotal={subtotal}/>
                    </Grid>
                </Grid>
            </Container>
        </>
    )
}

export default Index
