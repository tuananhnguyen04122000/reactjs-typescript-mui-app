import { Box, Divider, Typography } from '@mui/material'
import React, { FC } from 'react'
import { ICart } from '../../../recoil/Atoms'

interface PropsYourOrder {
    cart: ICart[]
    subtotal: number
}
const styleTitle = { mb: "16px", fontWeight: 700, lineHeight: 1.75, fontSize: 14 }

const styleBoxProductItem = { display: "flex", justifyContent: "space-between", mb: "12px" }

const styleBoxPrice = { display: "flex", gap: 0.5, alignItems: "center" }

const styleBoxSurcharge = { display: "flex", justifyContent: "space-between", mb: "5px" }

const styleTextPrice = { fontSize: 14, fontWeight: 700 }

const styleTextInfoProductItem = { fontSize: 14, color: "#3c4c68" }

const YourOrder: FC<PropsYourOrder> = (props) => {
    const { cart, subtotal } = props

    return (
        <Box sx={{ width: "100%" }}>
            <Typography sx={styleTitle}>Your order</Typography>
            {
                cart.map((productItem, index) => {
                    return (
                        <Box key={`${productItem}-${index}`} sx={styleBoxProductItem}>
                            <Box sx={styleBoxPrice}>
                                <Typography sx={styleTextPrice}>{productItem.quantity}</Typography>
                                <Typography sx={styleTextInfoProductItem}>x {productItem.name}</Typography>
                            </Box>
                            <Typography sx={styleTextInfoProductItem}>{`${productItem.totalPriceQuantity},00 US$`}</Typography>
                        </Box>
                    )
                })
            }
            <Divider sx={{ my: "24px" }} />
            
            <Box sx={styleBoxSurcharge}>
                <Typography sx={styleTextPrice}>Total:</Typography>
                <Typography sx={styleTextPrice}>{`${subtotal},00 US$`}</Typography>
            </Box>
        </Box>
    )
}

export default YourOrder
