import { Avatar, Box, Button, Card, CardContent, Checkbox, FormControl, FormControlLabel, Grid, InputLabel, MenuItem, Modal, styled, TextField, Typography } from "@mui/material"
import { fontSize } from "@mui/system"
import React, { useEffect, useState } from "react"
import { Controller, SubmitHandler, useForm } from "react-hook-form"
import { useRecoilState, useSetRecoilState } from "recoil"
import { cartState, ICart } from "../../../recoil/Atoms"

interface ICustomerInformation {
    fullName: string
    phoneNumber: string
    address: string
    city: string
    district: string
    ward: string
    street: string
}

const cardMonthYear = [
    { month: "January", year: 2021 },
    { month: "Febuary", year: 2022 },
    { month: "March", year: 2023 },
    { month: "April", year: 2024 },
    { month: "May", year: 2025 },
    { month: "June", year: 2026 },
    { month: "July", year: 2027 },
    { month: "August", year: 2028 },
    { month: "September", year: 2029 },
    { month: "October", year: 2030 },
    { month: "November", year: 2031 },
    { month: "December", year: 2032 }
]

const CssTextField = styled(TextField)({
    width: "100%",
    "& .MuiInputLabel-root": {
        marginTop: "-5px",
        fontSize: "14px",
    },
    '& label.Mui-focused': {
        color: "#d23f57",
    },
    "& .MuiOutlinedInput-root": {
        "&.Mui-focused fieldset": { borderColor: "#d23f57" }, borderColor: "#d23f57",
    },
    '& .MuiInputBase-input': {
        padding: '8.5px 14px',
    },
})

const CssMenuItem = styled(MenuItem)({
    "&.Mui-selected": {
        backgroundColor: "#fbf0f2",
        "&:hover": { backgroundColor: "#fbf0f2" }
    }
})
const styleCardContent = {
    pt: "24px",
    px: "28px"
}

const styleBoxTitle = {
    display: "flex",
    gap: "12px",
    alignItems: "center",
    mb: "28px"
}

const styleTextTitle = {
    fontWeight: 600,
    fontSize: 20
}
const styleTextCustomerInformation = {
    fontWeight: 400,
    fontSize: 14,
    mb: "12px"
}

const styleBoxInfoConfirm = {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    mt: 2
}

const styleBoxModal = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    bgcolor: 'background.paper',
    border: '5px solid #d23f57',
    borderRadius: 3,
    boxShadow: 24,
    p: 4,
};

const styleButtonPlaceOrder = {
    width: "100%",
    mt: "24px",
    ml: "24px",
    bgcolor: "#d23f57",
    textTransform: "none",
    ":hover": {
        bgcolor: "#E3364E"
    }
}

const styleButtonConfirm = {
    ...styleButtonPlaceOrder,
    width: "25%",
    m: 0,
}

const styleTextFieldConfirm = {
    fontSize: 16,
    fontWeight: 500,
    color: "#3c4c68"
}
const styleTextInfoConfirm = {
    fontSize: 16,
    fontWeight: 600,
}
const PaymentDetails = () => {
    const setCart = useSetRecoilState(cartState)
    const [customerInformation, setCustomerInformation] = useState<ICustomerInformation>({
        fullName: "",
        phoneNumber: "",
        address: "",
        city: "",
        district: "",
        ward: "",
        street: ""
    })
    const [openConfirmPayment, setOpenConfirmPayment] = React.useState(false);
    const { register, handleSubmit, formState: { errors }, control } = useForm<ICustomerInformation>();

    const handleClose = () => {
        const newCart: ICart[] = []
        setCart(newCart)
        localStorage.clear()
        setOpenConfirmPayment(false);
       
    }
    const onSubmit: SubmitHandler<ICustomerInformation> = (data: ICustomerInformation) => {
        setOpenConfirmPayment(true);
        setCustomerInformation(data)
    }
    return (
        <Card>
            <CardContent sx={styleCardContent}>
                <Box sx={styleBoxTitle}>
                    <Typography sx={styleTextTitle}>Payment Details</Typography>
                </Box>

                <Box>
                    <Typography sx={styleTextCustomerInformation}>Customer Information</Typography>
                </Box>

                <form onSubmit={handleSubmit(onSubmit)}>
                    <Grid container spacing={3} sx={{ mt: "-20px" }}>
                        <Grid item xs={12} sm={6} >
                            <Controller
                                name="fullName"
                                control={control}
                                rules={{ required: "Full name is required" }}
                                render={({ field }) => (
                                    <CssTextField
                                        {...field}
                                        required
                                        label="Enter Your Full Name"
                                        error={!!errors.fullName}
                                        helperText={errors.fullName && errors.fullName.message}
                                    />
                                )}
                            />
                        </Grid>

                        <Grid item xs={12} sm={6}>
                            <Controller
                                name="phoneNumber"
                                control={control}
                                rules={{
                                    required: "Phone Number is required",
                                    minLength: { value: 10, message: "Phone Number invalid" },
                                    maxLength: { value: 10, message: "Phone Number invalid" },
                                }}
                                render={({ field }) => (
                                    <CssTextField
                                        {...field}
                                        required
                                        type="number"
                                        label="Enter Your Phone Number"
                                        error={!!errors.phoneNumber}
                                        helperText={errors.phoneNumber && errors.phoneNumber.message}
                                    />
                                )}
                            />
                        </Grid>

                        <Grid item xs={12} sm={3}>
                            <Controller
                                name="city"
                                control={control}
                                rules={{ required: "City is required" }}
                                render={({ field }) => (
                                    <CssTextField
                                        {...field}
                                        required
                                        type="text"
                                        label="City"
                                        error={!!errors.city}
                                        helperText={errors.city && errors.city.message}
                                    />
                                )}
                            />
                        </Grid>

                        <Grid item xs={12} sm={3}>
                            <Controller
                                name="district"
                                control={control}
                                rules={{ required: "District is required" }}
                                render={({ field }) => (
                                    <CssTextField
                                        {...field}
                                        type="text"
                                        required
                                        label="District"
                                        error={!!errors.district}
                                        helperText={errors.district && errors.district.message}
                                    />
                                )}
                            />
                        </Grid>

                        <Grid item xs={12} sm={3}>
                            <Controller
                                name="ward"
                                control={control}
                                rules={{ required: "Ward is required" }}
                                render={({ field }) => (
                                    <CssTextField
                                        {...field}
                                        type="text"
                                        required
                                        label="Ward"
                                        error={!!errors.ward}
                                        helperText={errors.ward && errors.ward.message}
                                    />
                                )}
                            />
                        </Grid>

                        <Grid item xs={12} sm={3}>
                            <Controller
                                name="street"
                                control={control}
                                
                                rules={{ required: "Street is required" }}
                                render={({ field }) => (
                                    <CssTextField
                                        {...field}
                                        required
                                        label="Street"
                                        error={!!errors.street}
                                        helperText={errors.street && errors.street.message}
                                    />
                                )}
                            />
                        </Grid>

                        <Grid item xs={12} sm={12}>
                            <Controller
                                name="address"
                                control={control}
                                rules={{ required: "Address is required" }}
                                render={({ field }) => (
                                    <CssTextField
                                        {...field}
                                        required
                                        label="Enter Your Delivery Address"
                                        error={!!errors.address}
                                        helperText={errors.address && errors.address.message}
                                    />
                                )}
                            />
                        </Grid>
                        <Button type="submit" variant="contained" fullWidth sx={styleButtonPlaceOrder}>Place Order</Button>
                    </Grid>
                </form>

                <Modal
                    open={openConfirmPayment}
                    onClose={handleClose}
                >
                    <Box sx={styleBoxModal}>
                        <Typography sx={styleTextTitle}>Payment Infomation</Typography>
                        <Box sx={styleBoxInfoConfirm}>
                            <Typography sx={styleTextFieldConfirm}>Full name:</Typography>
                            <Typography sx={styleTextInfoConfirm}>{customerInformation.fullName}</Typography>
                        </Box>
                        <Box sx={styleBoxInfoConfirm}>
                            <Typography sx={styleTextFieldConfirm}>Phone number:</Typography>
                            <Typography sx={styleTextInfoConfirm}>{customerInformation.phoneNumber}</Typography>
                        </Box>
                        <Box sx={styleBoxInfoConfirm}>
                            <Typography sx={styleTextFieldConfirm}>Delivery Address:</Typography>
                            <Typography sx={styleTextInfoConfirm}>{customerInformation.address}</Typography>
                        </Box>
                        <Box sx={{ ...styleBoxInfoConfirm, mt: 4, justifyContent: "space-around" }}>
                            <Button variant="contained" sx={styleButtonConfirm} onClick={handleClose}>Close</Button>
                        </Box>
                    </Box>
                </Modal>

            </CardContent>
        </Card >
    )
}

export default PaymentDetails