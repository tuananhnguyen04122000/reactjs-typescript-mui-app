import React from 'react'
import Header from '../Header'
import Footer from '../Footer'
import ContentProduct from './ContentProduct'

const Index = () => {
    return (
        <>
            <Header />
            <ContentProduct />
            <Footer />
        </>
    )
}

export default Index
