import { Container } from '@mui/material'
import React from 'react'
import DetailsProduct from './DetailsProduct'
import TabsDescription from './tabs-description'

const Index = () => {
  return (
    <>
      <Container maxWidth={false} sx={{ width: 1280, height: 1330.97, my: "32px" }}>
        <DetailsProduct />
        <TabsDescription />
      </Container>
    </>

  )
}

export default Index
