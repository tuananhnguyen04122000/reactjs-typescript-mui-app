import { Box, Card, CardContent, Typography } from '@mui/material'
import React, { FC } from 'react'
import { IShop } from '.'

interface PropsShops{
    shops: IShop[],
    handleClick: () => void
}

const styleCard = { 
    minWidth: 255, 
    height: 406, 
}

const styleCardContentBody = {
    padding: "20px",
    "&:last-child": { paddingBottom: "20px" }
}

const styleBoxHeader = {
    display: "flex",
    mt: "-8px",
    mb: "8px",
    justifyContent: "space-between"
}

const styleTextBrands = {
    fontSize: "20px",
    fontWeight: 600,
    lineHeight: 1.5,
    py: "8px",
    px: "16px",
    color: "#7D879C",
    textTransform: "none",
    whiteSpace: "normal",
    cursor: "pointer"
}

const styleSeparatorCharacter = {
    fontSize: "20px",
    fontWeight: 600,
    lineHeight: 1.3,
    pt: "8px",
    color: "#DAE1E7",
    textTransform: "none",
    whiteSpace: "normal",
}

const styleTextShops = {
    ...styleTextBrands,
    color: "#2B3445",
}

const styleCardContentShopItem = {
    display: "flex",
    width: 180,
    height: 20,
    mb: "12px",
    bgcolor: "#F6F9FC",
    py: "12px",
    color: "#2B3445",
    borderRadius: "5px",
    gap: "1rem",
    alignItems: "center",
    "&:last-child": { paddingBottom: "12px" }
}

const styleTextShopItem = {
    fontSize: 17,
    fontWeight: 600,
    lineHeight: 1,
    textTransform: "capitalize",
    whiteSpace: "pre"
}

const styleCardContentViewAllBrands = {
    width: 168,
    height: 20,
    bgcolor: "#F6F9FC",
    borderRadius: "5px",
    pt: "12px",
    "&:last-child": { paddingBottom: "12px" },
    mt: "64px"
}

const styleTextViewAllBrands = {
    fontSize: 17,
    fontWeight: 600,
    lineHeight: 1,
    textTransform: "capitalize",
    textAlign: "center"
}

const Shops: FC<PropsShops> = (props) => {
    const { shops, handleClick } = props
  return (
    <Card sx={styleCard}>
            <CardContent sx={styleCardContentBody}>
                <Box sx={styleBoxHeader}>
                    <Typography variant="h3" sx={styleTextBrands} onClick={handleClick}>Brands</Typography>
                    <Typography variant="h3" sx={styleSeparatorCharacter}>|</Typography>
                    <Typography variant="h3" sx={styleTextShops}>Shops</Typography>
                </Box>

                {shops.map((shopItem) => {
                    return (
                        <CardContent sx={styleCardContentShopItem}>
                            <Box sx={{ width: 20, height: 20 }}>
                                <img style={{ width: "100%", height: "100%" }} src={shopItem.imgUrl} />
                            </Box>
                            <Typography variant="h4" sx={styleTextShopItem}>{shopItem.name}</Typography>
                        </CardContent>
                    )
                })}
                <CardContent sx={styleCardContentViewAllBrands}>
                    <Typography variant="h4" sx={styleTextViewAllBrands}>View All Brands</Typography>
                </CardContent>
            </CardContent>
        </Card>
  )
}

export default Shops
