import { Box, Card, CardContent, Typography } from '@mui/material'
import React, { FC } from 'react'
import { IBrandsPhone } from '.'

interface PropsBrandsPhone {
    brandsPhones: IBrandsPhone[],
    handleClick: () => void
}

const styleCard = {
    minWidth: 240,
    height: 518,
}

const styleCardContentBody = {
    padding: "20px",
    "&:last-child": { paddingBottom: "20px" }
}

const styleBoxHeader = {
    display: "flex",
    mt: "-8px",
    mb: "8px",
    justifyContent: "space-between"
}

const styleTextBrands = {
    fontSize: "20px",
    fontWeight: 600,
    lineHeight: 1.5,
    py: "8px",
    px: "16px",
    color: "#2B34450",
    textTransform: "none",
    whiteSpace: "normal",
    cursor: "pointer"
}

const styleSeparatorCharacter = {
    fontSize: "20px",
    fontWeight: 600,
    lineHeight: 1.3,
    pt: "8px",
    color: "#DAE1E7",
    textTransform: "none",
    whiteSpace: "normal",
}

const styleTextShops = {
    ...styleTextBrands,
    color: "#7D879C",
}

const styleCardContentBrandPhoneItem = {
    display: "flex",
    width: 168,
    height: 20,
    mb: "12px",
    bgcolor: "#F6F9FC",
    py: "12px",
    color: "#2B3445",
    borderRadius: "5px",
    gap: "1rem",
    alignItems: "center",
    "&:last-child": { paddingBottom: "12px" }
}

const styleTextBrandPhoneItem = {
    fontSize: 17,
    fontWeight: 600,
    lineHeight: 1,
    textTransform: "capitalize"
}

const styleCardContentViewAllBrands = {
    width: 168,
    height: 20,
    bgcolor: "#F6F9FC",
    borderRadius: "5px",
    pt: "12px",
    "&:last-child": { paddingBottom: "12px" },
    mt: "64px"
}

const styleTextViewAllBrands = {
    fontSize: 17,
    fontWeight: 600,
    lineHeight: 1,
    textTransform: "capitalize",
    textAlign: "center"
}

const BrandsPhone: FC<PropsBrandsPhone> = (props) => {
    const { brandsPhones, handleClick } = props

    return (
        <Card sx={styleCard}>
            <CardContent sx={styleCardContentBody}>
                <Box sx={styleBoxHeader}>
                    <Typography variant="h3" sx={styleTextBrands}>Brands</Typography>
                    <Typography variant="h3" sx={styleSeparatorCharacter}>|</Typography>
                    <Typography variant="h3" sx={styleTextShops} onClick={handleClick}>Shops</Typography>
                </Box>

                {brandsPhones.map((brandPhoneItem, index) => {
                    return (
                        <CardContent key={`${brandPhoneItem.name}-${index}`} sx={styleCardContentBrandPhoneItem}>
                            <Box sx={{ width: 20, height: 20 }}>
                                <img style={{ width: "100%", height: "100%" }} src={brandPhoneItem.imgUrl} />
                            </Box>
                            <Typography variant="h4" sx={styleTextBrandPhoneItem}>{brandPhoneItem.name}</Typography>
                        </CardContent>
                    )
                })}
                <CardContent sx={styleCardContentViewAllBrands}>
                    <Typography variant="h4" sx={styleTextViewAllBrands}>View All Brands</Typography>
                </CardContent>
            </CardContent>
        </Card>
    )
}

export default BrandsPhone
