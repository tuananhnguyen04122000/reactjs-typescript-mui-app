import { Box, Container, Grid, Typography } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import FiberNewRoundedIcon from '@mui/icons-material/FiberNewRounded';
import CardNewArrivals from './card-newarrivals';
import axios from 'axios';

export interface IProductNewArrival {
    imgUrl?: string
    name?: string
    price?: number
    id: string
}

const Index = () => {

    // const productNewArrivals: IProductNewArrival[] = [
    //     {
    //         imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fimagegoggles.png&w=1920&q=75", name: "Sunglass", price: 150
    //     },
    //     {
    //         imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Flipstick%20(2).png&w=1920&q=75", name: "Makeup", price: 250
    //     },
    //     {
    //         imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fbgwatch.png&w=1920&q=75", name: "Smart Watch", price: 350
    //     },
    //     {
    //         imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Flipstick%20(1).png&w=1920&q=75", name: "Lipstck", price: 15
    //     },
    //     {
    //         imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Flipstick%20(4).png&w=1920&q=75", name: "Green pant", price: 55
    //     },
    //     {
    //         imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Flipstick%20(3).png&w=1920&q=75", name: "Bonsai tree", price: 55
    //     },
    // ]
    const [productNewArrivals, setProductNewArrivals] = useState<IProductNewArrival[]>([])

    useEffect(() => {
        fetchData()
    }, [])

    async function fetchData() {
        try {
            await axios.get('https://6402f090f61d96ac4873acc5.mockapi.io/api/product').then((result) => {
                setProductNewArrivals(result.data[0].newArrivals);
            });

        } catch (errors) {
            console.log(errors)
        }
    }
        return (
            <Box mb={"60px"}>
                <Container maxWidth={false} sx={{ width: 1280, height: 331 }}>
                    <Box sx={{ display: "grid", width: 1232, height: 25, mb: "24px", gridTemplateAreas: `"new-arrivals view-all"` }}>
                        <Box sx={{
                            display: "flex",
                            gridArea: "new-arrivals",
                            alignItems: "center",
                            gap: "8px"
                        }}>
                            <FiberNewRoundedIcon sx={{ color: "#68c944" }} />
                            <Typography fontSize={25} sx={{ fontWeight: 700, lineHeight: 1 }}>New Arrivals</Typography>
                        </Box>
                        <Link to={"/"} style={{
                            gridArea: "view-all",
                            height: 21,
                            alignSelf: "center",
                            justifySelf: "end",
                            textDecoration: "none"
                        }}>
                            <Box display={"flex"} alignItems={"center"}>
                                <Typography fontSize={14}>View all</Typography>
                                <ArrowRightIcon fontSize="inherit" />
                            </Box>
                        </Link>
                    </Box>

                    <CardNewArrivals productNewArrivals={productNewArrivals} />
                </Container>
            </Box>
        )
    }

    export default Index
