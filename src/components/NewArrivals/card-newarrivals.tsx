import { Box, Card, CardContent, Grid, Typography } from '@mui/material';
import React, { FC } from 'react'
import { Link } from 'react-router-dom';
import { IProductNewArrival } from '.'

interface CardNewArrivalsProp {
    productNewArrivals: IProductNewArrival[]
}

const CardNewArrivals: FC<CardNewArrivalsProp> = (props) => {
    const { productNewArrivals } = props; 

    return (
        <Card>
            <CardContent sx={{ width: 1200, height: 234 }}>
                <Grid container spacing={3} sx={{ height: 258 }}>
                    {productNewArrivals.map((productNewArrivalItem, index) => {
                        return (
                            <Grid key={`${productNewArrivalItem.name}-${index}`} xs={6} sm={4} md={3} lg={2} sx={{ pt: "24px", pl: "24px" }}>
                                <Link to={`/product/${productNewArrivalItem.id}`} style={{ textDecoration: "none" }}>
                                    <Box sx={{ width: 180, height: 180, mb: "8px", ":hover": { filter: 'brightness(70%)', transition: "filter 0.4s" } }}>
                                        <img src={productNewArrivalItem.imgUrl} style={{ width: "100%", height: "100%" }} />
                                    </Box>
                                    <Box component={"h4"} sx={{ height: 21, mt: "0px", mb: "4px" }}>
                                        <Typography sx={{ fontSize: 14, fontWeight: 500 }}>{productNewArrivalItem.name}</Typography>
                                    </Box>
                                    <Box component={"h4"} sx={{ height: 21, mt: "0px", mb: "0px" }}>
                                        <Typography sx={{ color: "#D23F57", fontSize: 14, lineHeight: 1.5, fontWeight: 450, textTransform: "none", whiteSpace: "normal" }}>{productNewArrivalItem.price},00 US$</Typography>
                                    </Box>
                                </Link>
                            </Grid>
                        )
                    })}
                </Grid>
            </CardContent>
        </Card>
    )
}

export default CardNewArrivals
