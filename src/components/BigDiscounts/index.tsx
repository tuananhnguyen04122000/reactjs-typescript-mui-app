import { Box, Container } from '@mui/system'
import CardGiftcardIcon from '@mui/icons-material/CardGiftcard';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import React, { useEffect, useState } from 'react'
import { Typography } from '@mui/material';
import { Link } from 'react-router-dom';
import CardProductBigDiscounts from './card-bigdiscount';
import axios from 'axios';

export interface IProducBigDiscount {
    imgUrl?: string
    name?: string
    price?: number
    oldPrice?: number
    id?: string
}

const Index = () => {
    // const producBigDiscounts: IProducBigDiscount[] = [
    //     {
    //         imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F7.beatsw3.png&w=1920&q=75", name: "BenX 2020", price: 209, oldPrice: 233
    //     },
    //     {
    //         imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F9.SonyTV1080p.png&w=1920&q=75", name: "Tony TV 1080p", price: 258, oldPrice: 278
    //     },
    //     {
    //         imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F10.SonyPS4.png&w=1920&q=75", name: "Setgearr 2020", price: 116, oldPrice: 124
    //     },
    //     {
    //         imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F12.SonyBGB.png&w=1920&q=75", name: "Tony BGB", price: 269, oldPrice: 284
    //     },
    //     {
    //         imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F13.LGProducts.png&w=1920&q=75", name: "RG Products", price: 270, oldPrice: 300
    //     },
    //     {
    //         imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F14.Panasonic2019.png&w=1920&q=75", name: "Ranasonic 2019", price: 127, oldPrice: 137
    //     },
    //     {
    //         imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F15.DuneHD.png&w=1920&q=75", name: "Pune HD", price: 105, oldPrice: 111
    //     },
    //     {
    //         imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F16.SonyCCTV.png&w=1920&q=75", name: "Tont CCTV", price: 139, oldPrice: 150
    //     },
    // ]

    const [producBigDiscounts, setProducBigDiscounts] = useState<IProducBigDiscount[]>([])

    useEffect(() => {
        fetchData()
    }, [])

    async function fetchData() {
        try {
            await axios.get('https://6402f090f61d96ac4873acc5.mockapi.io/api/product').then((result) => {
                setProducBigDiscounts(result.data[0].bigDiscounts);
            });

        } catch (errors) {
            console.log(errors)
        }
    }

    return (
        <Box mb={"60px"}>
            <Container maxWidth={false} sx={{ width: 1280, height: 304.325 }}>
                <Box sx={{ display: "grid", width: 1232, height: 25, mb: "24px", gridTemplateAreas: `"big-discounts view-all"` }}>
                    <Box sx={{
                        display: "flex",
                        gridArea: "big-discounts",
                        alignItems: "center",
                        gap: "8px"
                    }}>
                        <CardGiftcardIcon sx={{ color: "#d23f57" }} />
                        <Typography fontSize={25} sx={{ fontWeight: 700, lineHeight: 1 }}>Big Discounts</Typography>
                    </Box>
                    <Link to={"/"} style={{
                        gridArea: "view-all",
                        height: 21,
                        alignSelf: "center",
                        justifySelf: "end",
                        textDecoration: "none"
                    }}>
                        <Box display={"flex"} alignItems={"center"}>
                            <Typography fontSize={14}>View all</Typography>
                            <ArrowRightIcon fontSize="inherit" />
                        </Box>
                    </Link>
                </Box>

                <CardProductBigDiscounts producBigDiscounts={producBigDiscounts} />
            </Container>
        </Box>
    )
}

export default Index
