import { Box, Card, CardContent, Typography } from '@mui/material';
import React, { FC } from 'react'
import { Link } from 'react-router-dom';
import Slider from 'react-slick';
import { IProducBigDiscount } from '.'
import SampleNextArrow from './arrow-button-custom/SampleNextArrow';
import SamplePrevArrow from './arrow-button-custom/SamplePrevArrow';

interface CardProductBigDiscountsProps {
  producBigDiscounts: IProducBigDiscount[];
}

const CardProductBigDiscounts: FC<CardProductBigDiscountsProps> = (props) => {
  const { producBigDiscounts } = props;

  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 6,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    swipeToSlide: true,
    touchThreshold: 100,
  };

  return (
    <Box>
      <Slider {...settings} className={"slick-track-bigdiscount"}>
        {producBigDiscounts.map((producBigDiscountItem, index) => {
          return (
            <Card key={`${producBigDiscountItem.name}-${index}`} sx={{ maxWidth: 185.325, maxHeight: 239.33 }}>
              <CardContent>
                <Link to={`/product/${producBigDiscountItem.id}`} style={{ textDecoration: "none" }}>
                  <Box sx={{ width: 153.325, height: 153.325, mb: "8px", ":hover": { filter: 'brightness(70%)', transition: "filter 0.4s" } }}>
                    <img style={{ width: "100%", height: "100%" }} src={producBigDiscountItem.imgUrl} />
                  </Box>

                  <Box component={"h4"} sx={{ height: 21, mt: "0px", mb: "4px" }}>
                    <Typography sx={{ fontSize: 14, fontWeight: 500 }}>{producBigDiscountItem.name}</Typography>
                  </Box>

                  <Box display={"flex"} gap={"8px"}>
                    <Typography component={"h4"} sx={{ color: "#D23F57", fontSize: 14, lineHeight: 1.5, fontWeight: 600, textTransform: "none", whiteSpace: "normal" }}>{producBigDiscountItem.price} US$</Typography>
                    <Typography component={"h4"} sx={{ color: "#7D879C", fontSize: 14, lineHeight: 1.5, fontWeight: 600, textTransform: "none", whiteSpace: "normal" }} ><del>{producBigDiscountItem.oldPrice} US$</del></Typography>
                  </Box>
                </Link>
              </CardContent>
            </Card>
          )})}
      </Slider>
    </Box>
  )
}

export default CardProductBigDiscounts
