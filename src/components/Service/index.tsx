import { Container, Grid, Icon } from '@mui/material'
import React from 'react'
import Services from './services'

import LocalShippingOutlinedIcon from '@mui/icons-material/LocalShippingOutlined';
import CreditScoreOutlinedIcon from '@mui/icons-material/CreditScoreOutlined';
import VerifiedUserOutlinedIcon from '@mui/icons-material/VerifiedUserOutlined';
import SupportAgentOutlinedIcon from '@mui/icons-material/SupportAgentOutlined';

export interface IService {
    icon?: any
    title?: string
    content?: string
}

const Index = () => {
    const services: IService[] = [
        {
            icon: <LocalShippingOutlinedIcon fontSize="inherit" />,
            title: "Worldwide Delivery",
            content: "We offer competitive prices on our 100 million plus product any range."
        },
        {
            icon: <CreditScoreOutlinedIcon fontSize="inherit"/>,
            title: "Safe Payment",
            content: "We offer competitive prices on our 100 million plus product any range."
        },
        {
            icon: <VerifiedUserOutlinedIcon fontSize="inherit"/>,
            title: "Shop With Confidence",
            content: "We offer competitive prices on our 100 million plus product any range."
        },
        {
            icon: <SupportAgentOutlinedIcon fontSize="inherit"/>,
            title: "24/7 Support",
            content: "We offer competitive prices on our 100 million plus product any range."
        },
    ]

    return (
        <Container maxWidth={false} sx={{ width: 1280, height: 302.5, mb: "70px" }}>
            <Services services={services} />
        </Container>
    )
}

export default Index
