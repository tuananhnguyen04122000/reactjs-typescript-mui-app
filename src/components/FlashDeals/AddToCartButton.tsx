import { Box, Button, Typography } from '@mui/material'
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import { FC, useState } from 'react'
import { useRecoilState, useRecoilValue } from 'recoil';
import { IProductFlashDeal } from '.';
import { shoppingBagState, ICart, cartState, IProduct } from '../../recoil/Atoms';
import { useAddToCart, useRemoveProductItemQuantity } from '../../recoil/Hooks';

interface PropsAddToCartButton {
    item: IProductFlashDeal;
}

const AddToCartButton: FC<PropsAddToCartButton> = (props) => {
    const { item } = props

    const styleBoxAddToCart = {
        display: "flex",
        flexDirection: "column-reverse",
        justifyContent: "space-between",
        alignItems: "center"
    }

    const styleButton = {
        minWidth: 27.6,
        height: 27.6,
        p: "3px",
        color: "#d23f57",
        borderColor: "#d23f57",
        ":hover": {
            borderColor: "#d23f57",
            bgcolor: "#fdf7f8",
        }
    }

    const styleCount = {
        fontWeight: 600,
        fontSize: 14,
        lineHeight: 1.5
    }
    
    //Convert productFlashDeal => product
    const product: IProduct = {
        imgUrl: item.imgUrl,
        name: item.name,
        price: item.price,
        id: item.id
    }

    const cartStateData = useRecoilValue<ICart[]>(cartState)
    const quantity = cartStateData?.find((productItem) => productItem.id === item.id)?.quantity
    const addToCart = useAddToCart()
    const removeProductItemQuantity = useRemoveProductItemQuantity()

    return (
        <Box sx={styleBoxAddToCart}>
            <Button variant="outlined" sx={styleButton} onClick={() => {
                addToCart(product)
            }}>
                <AddIcon fontSize="small" />
            </Button>
            {
                quantity && (
                    <>
                        <Typography variant='h3' sx={styleCount}>{quantity}</Typography>
                        <Button variant="outlined" sx={styleButton} onClick={() => removeProductItemQuantity(product)}>
                            <RemoveIcon fontSize="small" />
                        </Button>
                    </>
                )
            }

        </Box>
    )
}

export default AddToCartButton
