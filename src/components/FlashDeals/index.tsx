import { Container, Typography } from '@mui/material'
import { Box } from '@mui/system'
import BoltIcon from '@mui/icons-material/Bolt';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import { Link } from 'react-router-dom';
import CardProduct from './card-product';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { useRecoilState, useSetRecoilState } from 'recoil';
import { cartState, ICart } from '../../recoil/Atoms';

export interface IProductFlashDeal {
    imgUrl?: string
    name?: string
    price: number
    oldPrice?: number
    discount?: number
    id: string
}
const Index = () => {
    const [productFlashDeals, setProductFlashDeals] = useState<IProductFlashDeal[]>([])
    const setCart = useSetRecoilState<ICart[]>(cartState)

    useEffect(() => {
        fetchData()
        const storedCart = localStorage.getItem('cart')
        storedCart && setCart(JSON.parse(storedCart))
    }, [])

    async function fetchData() {
        await axios.get('https://6402f090f61d96ac4873acc5.mockapi.io/api/product').then((result) => {
            setProductFlashDeals(result.data[0].flashDeals);
        });
    }
    return (

        <Box width={1519.2} height={474} mb={"60px"} >
            <Container maxWidth={false} sx={{ width: 1280, height: 474 }}>
                <Box sx={{ display: "grid", width: 1232, height: 25, mb: "24px", gridTemplateAreas: `"flash-deals view-all"` }}>
                    <Box sx={{
                        display: "flex",
                        gridArea: "flash-deals",
                        height: 25,
                        alignItems: "center",
                        gap: 1
                    }}>
                        <BoltIcon color="error" />
                        <Typography fontSize={25} sx={{ fontWeight: 700, lineHeight: 1 }}>Flash Deals</Typography>
                    </Box>
                    <Link to={"/"} style={{
                        gridArea: "view-all",
                        height: 21,
                        alignSelf: "center",
                        justifySelf: "end",
                        textDecoration: "none"
                    }}>
                        <Box display={"flex"} alignItems={"center"}>
                            <Typography fontSize={14}>View all</Typography>
                            <ArrowRightIcon fontSize="inherit" />
                        </Box>
                    </Link>
                </Box>
                {productFlashDeals && productFlashDeals?.length > 0 &&
                    <CardProduct productFlashDeals={productFlashDeals} />
                }
            </Container>
        </Box>
    )
}


export default Index
