import { FC } from "react";
import { IProductFlashDeal } from ".";
import Slider from "react-slick";
import Card from "@mui/material/Card";
import { Button, CardContent, CardHeader, Chip, IconButton, Rating, Typography } from "@mui/material";
import Box from "@mui/system/Box";
import SamplePrevArrow from "./arrow-button-custom/SamplePrevArrow";
import SampleNextArrow from "./arrow-button-custom/SampleNextArrow";
import { Link } from "react-router-dom";
import { style } from "@mui/system";
import AddToCartButton from "./AddToCartButton";

interface CardProductProps {
    productFlashDeals: IProductFlashDeal[]
}

const CardProduct: FC<CardProductProps> = (props) => {
    const { productFlashDeals } = props

    const styleCard = {
        maxWidth: 290,
        height: 401
    }
    
    const styleChip = {
        position: "absolute",
        fontWeight: 600,
        fontSize: 10,
        cursor: "default",
        mt: 1,
        ml: 1,
        zIndex: 1,
    }
    
    const styleImage = {
        width: 290,
        height: 290
    }
    
    const styleCardContent = {
        display: "flex",
        height: 79
    }
    
    const styleBoxContent = {
        width: 220,
        height: 79,
        mr: "8px"
    }
    
    const styleBoxPrice = {
        display: "flex",
        gap: "8px",
        mt: "4px"
    }
    
    const styleTextPrice = {
        color: "#D23F57",
        fontSize: 14,
        lineHeight: 1.5,
        fontWeight: 600,
        textTransform: "none",
        whiteSpace: "normal"
    }
    
    const styleTextOldPrice = {
        ...styleTextPrice,
        color: "#7D879C"
    }
    
    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />,
        swipeToSlide: true,
        touchThreshold: 100,
    };

    return (
        <Slider {...settings} className={"slick-track-flasddeals"}>
            {productFlashDeals.map((productFlashDealItem, index) => {
                return (
                    <Card sx={styleCard} key={`${productFlashDealItem}-${index}`}>
                        <Link to={`/product/${productFlashDealItem.id}`} style={{ textDecoration: "none" }}>
                            <Chip
                                label={`${productFlashDealItem.discount}% off`}
                                color={"error"}
                                size={"small"}
                                sx={styleChip}
                            />
                            <img style={{ ...styleImage, position: "relative" }} src={`${productFlashDealItem.imgUrl}`} />
                        </Link>
                        <CardContent sx={styleCardContent}>
                            <Box sx={styleBoxContent}>
                                <Typography fontSize={14} component="div" mb={"8px"}>{productFlashDealItem.name}</Typography>
                                <Rating defaultValue={5} sx={{ fontSize: "1.25rem" }} readOnly />
                                <Box sx={styleBoxPrice}>
                                    <Typography component="div" sx={styleTextPrice}>{productFlashDealItem.price} US$</Typography>
                                    <Typography component="div" sx={styleTextOldPrice}><del>{productFlashDealItem.oldPrice} US$</del></Typography>
                                </Box>
                            </Box>
                            
                            <AddToCartButton item={productFlashDealItem} />
                        </CardContent>
                    </Card>
                )
            })
            }
        </Slider>
    )
}
export default CardProduct

