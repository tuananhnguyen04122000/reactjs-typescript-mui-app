import { IconButton } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import React, { FC } from "react"

interface IStylePrevArrow {
  onClick?: any
}

const SamplePrevArrow: FC<IStylePrevArrow> = (props) => {
  const { onClick } = props;

  return (
    <IconButton onClick={onClick} sx={{
      position: "absolute",
      width: "40px",
      height: "40px",
      backgroundColor: "#0F3460",
      top: 180,
      left: "-15px",
      zIndex: 1,
      "&:hover": {
        backgroundColor: "#0F3460",
      },
    }}>
      <ArrowBackIcon fontSize="small" sx={{ color: "white" }} />
    </IconButton >
  );
}

export default SamplePrevArrow
