import IconButton from '@mui/material/IconButton';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import React, { FC } from 'react'

interface IStyleNextArrow {
    onClick?: any
}

const SampleNextArrow: FC<IStyleNextArrow> = (props) => {
    const { onClick } = props;
    return (
        <IconButton onClick={onClick} sx={{
            position: "absolute",
            width: "40px",
            height: "40px",
            backgroundColor: "#0F3460",
            top: 55,
            right: "-10px",
            zIndex: 1,
            "&:hover": {
              backgroundColor: "#0F3460",
            },
          }}>
            <ArrowForwardIcon fontSize="small" sx={{ color: "white" }} />
          </IconButton >
    );
}

export default SampleNextArrow
