import React from "react";
import { Route, Routes } from "react-router-dom";
import Layout from "./components/Layout";
import Checkout from "./components/Layout/Checkout";
import Product from "./components/Layout/Product";
import logo from "./logo.svg";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Layout />} />
        <Route path="/product/:id" element={<Product />} />
        <Route path="/checkout" element={<Checkout />} />
      </Routes>
    </div>
  );
}

export default App;
