import { useRecoilState } from "recoil"
import { saveCartToLocalStorage } from "../localstorage/localStorage";
import { cartState, ICart, IProduct } from "./Atoms"

export const useAddToCart = () => {
    const [cartData, setCart] = useRecoilState<ICart[]>(cartState)
    const newCart: ICart[] = [...cartData];

    saveCartToLocalStorage(cartData);

    return (product: IProduct) => {
        const foundIndex: number = cartData.findIndex(x => x.id === product.id);
        if (foundIndex >= 0) {
            newCart[foundIndex] = {
                ...cartData[foundIndex],
                quantity: cartData[foundIndex].quantity + 1,
                totalPriceQuantity: cartData[foundIndex].totalPriceQuantity + cartData[foundIndex].price
            };
        }
        else {
            newCart.push({
                ...product,
                id: product.id,
                quantity: 1,
                totalPriceQuantity: product.price
            })
        }
        setCart(newCart);
    }

}

export const useRemoveProductItemQuantity = () => {
    const [cartData, setCart] = useRecoilState<ICart[]>(cartState)
    const newCart: ICart[] = [...cartData];

    saveCartToLocalStorage(cartData);
    return (product: IProduct) => {
        const foundIndex: number = cartData.findIndex(x => x.id === product.id);

        if (cartData[foundIndex].quantity === 1) {
            newCart.splice(foundIndex, 1)
        }
        else if (foundIndex >= 0) {
            newCart[foundIndex] = {
                ...cartData[foundIndex],
                quantity: cartData[foundIndex].quantity - 1,
                totalPriceQuantity: cartData[foundIndex].totalPriceQuantity - cartData[foundIndex].price
            };
        }
        setCart(newCart);
    }
}

export const useRemoveAllProducts = () => {
    const [cartData, setCart] = useRecoilState<ICart[]>(cartState)
    const newCart: ICart[] = [...cartData];

    saveCartToLocalStorage(cartData);
    return (product: IProduct) => {
        const productIndex: number = cartData.findIndex(x => x.id === product.id);

        if (productIndex >= 0 && cartData.length > 0) {
            newCart.splice(productIndex, 1)
            setCart(newCart)
        }
        setCart(newCart);
    }
}